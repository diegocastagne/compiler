/* 
    Linked list example from https://www.programiz.com/dsa/linked-list-operations
    Modified by:
    A01329863 Diego Castagne Artezan 
    A01730256 Melissa Morales Merino
*/
#ifndef _SYMBOLTABLE_
#define _SYMBOLTABLE_


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "helper.h"

typedef struct LinkedList {
    char * name;
    struct HValue * value;
    struct LinkedList* next;
} LinkedList;

typedef struct LLTable {
    char * name;
    struct HValue * value;
    struct FunctionData * function;
    struct LLTable * next;
} LLTable;

typedef struct FunctionData {
    bool isFunction;
    int args;
    struct LinkedList* list;
    struct TreeNode * node;
    struct LLTable * table;
} FunctionData;


void InitializeList(LLTable *);
void InsertIdentifier(LLTable**, char *);
void assignType(LLTable**, int, HValue *);
void deleteSymbol(LLTable**, char *);
bool checkIdentifier(LLTable**, char *);
void assignIdentifierValue(LLTable**, char *, HValue *);
HValue * getValueFromList(LLTable** , char * );
void deleteList(LLTable**);
void printList(LLTable*);
bool CheckIfFunction(LLTable *, char *);
bool CheckParamCount(LLTable *, char *, int);
int GetArgumentsFunction(LLTable *, char *);


#endif