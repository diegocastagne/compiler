/* 
    A01329863 Diego Castagne Artezan 
    A01730256 Melissa Morales Merino
*/
#ifndef _HELPER_
#define _HELPER_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef union HNumber {
    int intV;
    float floatV;
    bool boolV;
} HNumber;
typedef struct HValue {
    char type;
    union HNumber number;
} HValue;


#define HINTEGER 30
#define HFLOAT   31
#define HBOOL    32
#define HSUM     33
#define HSUB     34
#define HMULT    35
#define HDIV     36
#define HLESS    37
#define HGREATER 38
#define HEQUALS  39
#define HBOR     40
#define HEXCLAM  41
#define HAMPER   42


HValue * Operation(HValue*, HValue*, char);
HValue * CreateInteger(int);
HValue * CreateFloat(float);
HValue * CreateBool(bool);
bool CheckTypesValues(HValue*, HValue*);

#endif