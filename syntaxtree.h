/* 
    A01329863 Diego Castagne Artezan 
    A01730256 Melissa Morales Merino
*/
#include "helper.h"
#include "linkedlist.h"

#define TINSTRUCTION  10
#define TIDENTIFIER   11
#define TVALUE        12
#define TSTMT         13
#define TASSIGN       14
#define TIF           15
#define TIFELSE       16
#define TWHILE        17
#define TREPEAT       18
#define TREAD         19
#define TPRINT        20
#define TEXPR         21
#define TTERM         22
#define TRETURN       23
#define TFUNCTION     24
#define TARG          25
#define TNULLV        NULL
#define TNULLT        '\0'



typedef struct TreeNode {
    char type;
    char operation;
    char instruction;
    char * name;
    struct HValue * value;
    struct TreeNode * nodea;
    struct TreeNode * nodeb;
    struct TreeNode * nodec;
} TreeNode;

HValue * GValue;
LLTable * GTable;

TreeNode * InitializeTree();
TreeNode * CreateNodeTree(char, char, char, char*, HValue*, TreeNode*, TreeNode*, TreeNode*);
void RunNode(TreeNode*);
void RunInstruction(TreeNode*);
TreeNode * CreateExpr(char, TreeNode*, TreeNode*, TreeNode*);
void RunExpr(TreeNode*);
TreeNode * CreateTerm(char, TreeNode*, TreeNode*, TreeNode*);
void RunTerm(TreeNode*);
TreeNode * CreateValueTree(char, char*, HValue*);
void RunIdentifier(TreeNode *);
TreeNode * CreateStmt(TreeNode*, TreeNode*, TreeNode*);
void RunStmt(TreeNode*);
TreeNode * CreateAssign(TreeNode*, TreeNode*, TreeNode*);
void RunAssign(TreeNode*);
TreeNode * CreateIf(TreeNode*, TreeNode*, TreeNode*);
void RunIf(TreeNode*);
TreeNode * CreateIfElse(TreeNode*, TreeNode*, TreeNode*);
void RunIfElse(TreeNode*);
TreeNode * CreateWhile(TreeNode*, TreeNode*, TreeNode*);
void RunWhile(TreeNode*);
TreeNode * CreateRepeat(TreeNode*, TreeNode*, TreeNode*);
void RunRepeat(TreeNode*);
TreeNode * CreateRead(TreeNode*, TreeNode*, TreeNode*);
void RunRead(TreeNode*);
TreeNode * CreatePrint(TreeNode*, TreeNode*, TreeNode*);
void RunPrint(TreeNode*);
TreeNode * CreateFunction(char *, HValue *, TreeNode *);
void RunFunction(TreeNode*);
TreeNode * CreateArg(TreeNode*, TreeNode*, TreeNode*);
void RunArgs(TreeNode*, LinkedList*);
TreeNode * CreateReturn(TreeNode*, TreeNode*, TreeNode*);
void RunReturn(TreeNode*);


bool CheckTypesTree(TreeNode*, TreeNode*);
void InsertFunctionTable(LLTable **, char *, HValue *, LinkedList *, TreeNode *, LLTable *);
TreeNode * GetNodeTable(LLTable *, char *);
LinkedList * GetListTable(LLTable *, char *);
LLTable * getFunctionTable(LLTable *, char *);
int ParamCount(LinkedList *);
LinkedList * ParamJoin(LinkedList *, LinkedList *);
LinkedList * ParamCreate(char *, HValue *);
void concatenateTables(LLTable **, LLTable *);
void separateTables(LLTable **, LLTable * );
void InsertStmtFunction(LLTable **, char *, TreeNode *);