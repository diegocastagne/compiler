/* 
    A01329863 Diego Castagne Artezan 
    A01730256 Melissa Morales Merino
*/
#include "helper.h"

//This method operates two values depending on the character sent and their types
HValue * Operation(HValue * one, HValue * two, char operation){
    if (!CheckTypesValues(one, two)) return CreateInteger(0);
    if (one->type == HINTEGER) {
        int input_one = one->number.intV;
        int input_two = two->number.intV;
        int output = 0;

        switch (operation) {
            case HSUM:          output = input_one + input_two; break;
            case HSUB:    output = input_one - input_two; break;
            case HMULT:     output = input_one * input_two; break;
            case HDIV:       output = input_one / input_two; break;
            case HLESS:         return CreateBool(input_one < input_two);
            case HGREATER:      return CreateBool(input_one > input_two);
            case HEQUALS:       return CreateBool(input_one == input_two);
            default:                output = 0; break;
        }
        return CreateInteger(output);
    } else if (one->type == HFLOAT) {
        float input_one = one->number.floatV;
        float input_two = two->number.floatV;
        float output = 0;

        switch (operation) {
            case HSUM:          output = input_one + input_two; break;
            case HSUB:    output = input_one - input_two; break;
            case HMULT:     output = input_one * input_two; break;
            case HDIV:       output = input_one / input_two; break;
            case HLESS:         return CreateBool(input_one < input_two);
            case HGREATER:      return CreateBool(input_one > input_two);
            case HEQUALS:       return CreateBool(input_one == input_two);
            default:                output = 0; break;
        }
        return CreateFloat(output);
    } else if(one->type == HBOOL){
        bool output = false;
        float input_one = one->number.boolV;
        float input_two = two->number.boolV;

        switch (operation) {
            case HAMPER:        output = input_one && input_two; break;
            case HEXCLAM:       output = !input_one; break;
            case HBOR:          output = input_one || input_two; break;
            default:                output = false; break;
        }
        

        return CreateBool(output);

    }else return CreateInteger(0);
}

//Methods to create the three types of values
HValue * CreateInteger(int number) {
    HValue * value = (HValue *)calloc(1, sizeof(HValue));
    value->type = HINTEGER;
    value->number.intV = number;
    return value;
}

HValue * CreateFloat(float number) {
    HValue * value = (HValue *)calloc(1, sizeof(HValue));
    value->type = HFLOAT;
    value->number.floatV = number;
    return value;
}

HValue * CreateBool(bool number) {
    HValue * value = (HValue *)calloc(1, sizeof(HValue));
    value->type = HBOOL;
    value->number.boolV = number;
    return value;
}

// Method to check types
bool CheckTypesValues(HValue * one, HValue * two) {
    return one->type == two->type;
}