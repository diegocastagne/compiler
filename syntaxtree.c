/* 
    A01329863 Diego Castagne Artezan 
    A01730256 Melissa Morales Merino
*/
#include "syntaxtree.h"

TreeNode * InitializeTree(){
    TreeNode * node;
    node = (TreeNode *)malloc(sizeof(TreeNode));

    node->type = TNULLT;
    node->instruction = TNULLT;
    node->name  = TNULLV;
    node->value = TNULLV;
    node->nodea = TNULLV;
    node->nodeb = TNULLV;
    node->nodec = TNULLV;

    return node;
}

TreeNode * CreateNodeTree(char type, char operation, char instruction, char * name, HValue * value, TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    TreeNode * node;
    node = (TreeNode *)malloc(sizeof(TreeNode));

    node->type = type;
    node->operation = operation;
    node->instruction = instruction;
    node->name = name;
    node->value = value;
    node->nodea = nodea;
    node->nodeb = nodeb;
    node->nodec = nodec;

    return node;
}

// Executes the nodes depending on their types
void RunNode(TreeNode * node) {
    if (node == NULL) return;

    switch (node->type) {
        case TINSTRUCTION:
            RunInstruction(node);
            break;
        case TIDENTIFIER:
            RunIdentifier(node);
            break;
        case TVALUE:
            break;
        default:
            break;
    }
}

TreeNode * CreateExpr(char operation, TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    HValue * value;
    
    value = Operation(nodea->value, nodeb->value, operation);

    return CreateNodeTree(TINSTRUCTION, operation, TEXPR, TNULLV, value, nodea, nodeb, nodec);
}

// Evaluates the expr if the nodes are not epmty and then performs an operation with both
void RunExpr(TreeNode * node) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("Node a empty\n");
        exit(EXIT_FAILURE);
    } else if (node->nodeb == NULL) {
        printf("Node b empty\n");
        exit(EXIT_FAILURE);
    }

    RunNode(node->nodea);
    RunNode(node->nodeb);

    node->value = Operation(node->nodea->value, node->nodeb->value, node->operation);
}

TreeNode * CreateTerm(char operation, TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    HValue * value;
    value = Operation(nodea->value, nodeb->value, operation);

    return CreateNodeTree(TINSTRUCTION, operation, TTERM, TNULLV, value, nodea, nodeb, nodec);
}

// Executes the term if the nodes are not empty and then does an operation on them
void RunTerm(TreeNode * node) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("Node a empty\n");
        exit(EXIT_FAILURE);
    } else if (node->nodeb == NULL) {
        printf("Node b empty\n");
        exit(EXIT_FAILURE);
    }

    RunNode(node->nodea);
    RunNode(node->nodeb);

    node->value = Operation(node->nodea->value, node->nodeb->value, node->operation);
}

TreeNode * CreateValueTree(char type, char * name, HValue * value){
    return CreateNodeTree(type, TNULLT, TNULLT, name, value, TNULLV, TNULLV, TNULLV);
}

// Copies the identifier value and returns it via the node value
void RunIdentifier(TreeNode * node) {
    if (node == NULL) return;

    HValue * value = getValueFromList(&GTable, node->name);

    switch (value->type)
    {
        case HINTEGER:
            node->value = CreateInteger(value->number.intV);
            break;
        case HFLOAT:
            node->value = CreateFloat(value->number.floatV);
            break;
        case HBOOL:
            node->value = CreateBool(value->number.boolV);
            break;
    
    }
}

TreeNode * CreateStmt(TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    return CreateNodeTree(TINSTRUCTION, TNULLT, TSTMT, TNULLV, TNULLV, nodea, nodeb, nodec);
}

void RunStmt(TreeNode * node) {
    if (node == NULL) return;

    RunNode(node->nodea);
    RunNode(node->nodeb);
}

TreeNode * CreateAssign(TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    return CreateNodeTree(TINSTRUCTION, TNULLT, TASSIGN, TNULLV, TNULLV, nodea, nodeb, nodec);
}

// Executes both nodes and does a type check, if succesful assignf the value from nodeb to nodea
void RunAssign(TreeNode * node) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("Node a empty\n");
        exit(EXIT_FAILURE);
    } else if (node->nodeb == NULL) {
        printf("Node b empty\n");
        exit(EXIT_FAILURE);
    }

    RunNode(node->nodea);
    RunNode(node->nodeb);

    HValue * nodea_value = node->nodea->value;
    HValue * nodeb_value = node->nodeb->value;

    if (nodea_value->type != nodeb_value->type) {
        printf("Nodes are different types in assign.\n");
        exit(EXIT_FAILURE);
    }

    nodea_value->number = nodeb_value->number;
    assignIdentifierValue(&GTable, node->nodea->name, nodeb_value);
}

TreeNode * CreateIf(TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    return CreateNodeTree(TINSTRUCTION, TNULLT, TIF, TNULLV, TNULLV, nodea, nodeb, nodec);
}

// Based on the evaluation of bool values in the expression runs nodeb or not
void RunIf(TreeNode * node) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("Node a empty\n");
        exit(EXIT_FAILURE);
    } else if (node->nodeb == NULL) {
        printf("Node b empty\n");
        exit(EXIT_FAILURE);
    }

    RunNode(node->nodea);

    if (node->nodea->value->number.boolV) RunNode(node->nodeb);
}

TreeNode * CreateIfElse(TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    return CreateNodeTree(TINSTRUCTION, TNULLT, TIFELSE, TNULLV, TNULLV, nodea, nodeb, nodec);
}

//Based on the evaluation of bool values in the expression runs nodeb or nodec
void RunIfElse(TreeNode * node) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("Node a empty\n");
        exit(EXIT_FAILURE);
    } else if (node->nodeb == NULL) {
        printf("Node b empty\n");
        exit(EXIT_FAILURE);
    } else if (node->nodec == NULL) {
        printf("node c empty\n");
        exit(EXIT_FAILURE);
    }

    RunNode(node->nodea);

    if (node->nodea->value->number.boolV) RunNode(node->nodeb);
    else RunNode(node->nodec);
}

TreeNode * CreateWhile(TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    return CreateNodeTree(TINSTRUCTION, TNULLT, TWHILE, TNULLV, TNULLV, nodea, nodeb, nodec);
}

void RunWhile(TreeNode * node) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("node a empty\n");
        exit(EXIT_FAILURE);
    } else if (node->nodeb == NULL) {
        printf("Node b empty\n");
        exit(EXIT_FAILURE);
    }

    RunNode(node->nodea);

    while (node->nodea->value->number.boolV) {
        RunNode(node->nodeb);
        RunNode(node->nodea);
    }
}

TreeNode * CreateRepeat(TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    return CreateNodeTree(TINSTRUCTION, TNULLT, TREPEAT, TNULLV, TNULLV, nodea, nodeb, nodec);
}

// This is basically a do while
void RunRepeat(TreeNode * node) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("Node a empty\n");
        exit(EXIT_FAILURE);
    } else if (node->nodeb == NULL) {
        printf("Node b empty\n");
        exit(EXIT_FAILURE);
    }

    RunNode(node->nodea);

    do{
        RunNode(node->nodeb);
        RunNode(node->nodea);
    }while (node->nodea->value->number.boolV);
}

TreeNode * CreateRead(TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    return CreateNodeTree(TINSTRUCTION, TNULLT, TREAD, TNULLV, TNULLV, nodea, nodeb, nodec);
}

// Depending on the data type it asks for the correct value
void RunRead(TreeNode * node) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("Node a empty\n");
        exit(EXIT_FAILURE);
    }

    char * nodea_name = node->nodea->name;
    HValue * nodea_value = node->nodea->value;
    char type = nodea_value->type;

    if (type == HINTEGER) {
        int input;
        printf("\nEnter the value of %s : ", nodea_name);
        scanf("%d", &input);
        nodea_value->number.intV = input;
        assignIdentifierValue(&GTable, node->nodea->name, node->nodea->value);
    } else if (type == HFLOAT) {
        float input;
        printf("\nEnter the value of %s :", nodea_name);
        scanf("%f", &input);
        nodea_value->number.floatV = input;
        assignIdentifierValue(&GTable, node->nodea->name, node->nodea->value);
    } else if (type == HBOOL) {
        bool input;
        int temp;
        printf("\nEnter the value of %s :", nodea_name);
        scanf("%d", &temp);
        input = temp;
        nodea_value->number.boolV = input;
        assignIdentifierValue(&GTable, node->nodea->name, node->nodea->value);
    } else {
        printf("unrecognized type\n");
        exit(EXIT_FAILURE);
    }
}

TreeNode * CreatePrint(TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec){
    return CreateNodeTree(TINSTRUCTION, TNULLT, TPRINT, TNULLV, TNULLV, nodea, nodeb, nodec);
}

// Prints the node value
void RunPrint(TreeNode * node) {
    if (node == NULL) return;

    // Update nodes.
    RunNode(node->nodea);

    if (node->nodea == NULL) {
        printf("Node a empty\n");
        exit(EXIT_FAILURE);
    }

    HValue * nodea_value = node->nodea->value;
    char type = nodea_value->type;

    if (type == HINTEGER)
        printf("\nvalue = %d\n", nodea_value->number.intV);
    else if (type == HFLOAT)
        printf("\nvalue = %f\n", nodea_value->number.floatV);
    else if (type == HBOOL)
        printf("\nvalue = %d\n", nodea_value->number.boolV);
    else {
        printf("unrecognized type\n");
        exit(EXIT_FAILURE);
    }
}

TreeNode * CreateFunction(char * name, HValue * value, TreeNode * nodea) {
    return CreateNodeTree(TINSTRUCTION, TNULLT, TFUNCTION, name, value, nodea, TNULLV, TNULLV);
}

void RunFunction(TreeNode * node) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("Function: The nodea is empty.\n");
        exit(EXIT_FAILURE);
    }

    // Connect function symbol table to global one
    LLTable * headHolder = GTable;
    concatenateTables(&GTable, getFunctionTable(GTable, node->name));

    // Updates the nodes and runs the function
    TreeNode * function_node;
    LinkedList * function_list;
    function_node = GetNodeTable(GTable, node->name);
    function_list = GetListTable(GTable, node->name);
    RunArgs(node->nodea, function_list);
    RunNode(function_node);
    if (GValue != NULL) node->value = GValue;
    assignIdentifierValue(&GTable, node->name, node->value);
    separateTables(&GTable, headHolder);
}

TreeNode * CreateArg(TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec) {
    return CreateNodeTree(TINSTRUCTION, TNULLT, TARG, TNULLV, TNULLV, nodea, nodeb, nodec);
}

// Goes through the args list running each one
void RunArgs(TreeNode * node, LinkedList * list) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("node a empty.\n");
        exit(EXIT_FAILURE);
    }
    
    if (list == NULL) {
        printf("not enough params.\n");
        exit(EXIT_FAILURE);
    }

    RunNode(node->nodea);
    assignIdentifierValue(&GTable, list->name, node->nodea->value);
    RunArgs(node->nodeb, list->next);
}

TreeNode * CreateReturn( TreeNode * nodea, TreeNode * nodeb, TreeNode * nodec) {
    return CreateNodeTree(TINSTRUCTION, TNULLT, TRETURN, TNULLV, TNULLV, nodea, nodeb, nodec);
}

// Changes the global value based on what is needed to return
void RunReturn(TreeNode * node) {
    if (node == NULL) return;

    if (node->nodea == NULL) {
        printf("Return: The nodea is empty.\n");
        exit(EXIT_FAILURE);
    }

    RunNode(node->nodea);
    node->value = node->nodea->value;
    GValue = node->nodea->value;
}

// Compares the two nodes types
bool CheckTypesTree(TreeNode * one, TreeNode * two) {
    if (one == NULL || two == NULL) return false;
    return one->value->type == two->value->type;
}

// Bases on what type of node is calls the correct function
void RunInstruction(TreeNode * node) {
    if (node == NULL) return;

    switch (node->instruction) {
        case TSTMT:
            RunStmt(node);
            break;
        case TASSIGN:
            RunAssign(node);
            break;
        case TIF:
            RunIf(node);
            break;
        case TIFELSE:
            RunIfElse(node);
            break;
        case TWHILE:
            RunWhile(node);
            break;
        case TREPEAT:
            RunRepeat(node);
            break;
        case TREAD:
            RunRead(node);
            break;
        case TPRINT:
            RunPrint(node);
            break;
        case TEXPR:
            RunExpr(node);
            break;
        case TTERM:
            RunTerm(node);
            break;
        case TFUNCTION:
            RunFunction(node);
            break;
        case TRETURN:
            RunReturn(node);
            break;
        default:
            break;
    }
}

// Grabs the function table and concatenates it at the start of the symbol table (they appear first in searches)
void concatenateTables(LLTable ** ref, LLTable * fun_table)
{
    if((*ref) == NULL || fun_table == NULL) return;

    LLTable * last = fun_table;
    while(last->next != NULL) last = last->next;

    last->next = (*ref);
    (*ref) = fun_table;
}

// Separates the function table from the global symbol table based on the original head
void separateTables(LLTable ** ref, LLTable * original_head){
    if((*ref) == NULL || original_head == NULL) return;

    LLTable * temp = (*ref);

    while(temp->next != original_head) temp = temp->next;

    temp->next = NULL;
    (*ref) = original_head;
}

// Inserts the function into the corresponding identifier
void InsertFunctionTable(LLTable ** table, char * name, HValue * value, LinkedList * list, TreeNode * node, LLTable * function_table){

    struct LLTable* new_node = (struct LLTable*)malloc(sizeof(struct LLTable));

    new_node->name = name;
    new_node->value = value;

    FunctionData * item = (FunctionData*)malloc(sizeof(FunctionData));
    item->isFunction = true;
    item->args = ParamCount(list);
    item->list = list;
    item->node = node;
    item->table = function_table;

    new_node->function = item;
    new_node->next = (*table);
    (*table) = new_node;

}

// Tried to do recursiveness to no avail
void InsertStmtFunction(LLTable ** table, char * name, TreeNode * node){
    LLTable * temp = (*table);

    while(temp->next != NULL){
        if(temp->name == name){
            temp->function->node = node;
            return;
        }
    }
}

// Counts the amount of parameter a function has
int ParamCount(LinkedList * node){
    if(node == NULL) return 0;

    int count = 1;

    while(node->next != NULL)
    {
        count++;
        node = node->next;
    }

    return count;
}

// Creates the linked list for the parameters
LinkedList * ParamJoin(LinkedList * one, LinkedList * two){
    one->next = two;
    return one;
}

// Creates a parameter
LinkedList * ParamCreate(char * name, HValue * value) {
    LinkedList * list;
    list = (LinkedList *)calloc(1, sizeof(LinkedList));
    list->name = name;
    list->value = value;
    list->next = NULL;
    return list;
}

// Gets the parameter list based on the identifier in the symbol table
LinkedList * GetListTable(LLTable * table, char * name) {
    struct LLTable *temp = table, *prev;

    while (temp != NULL) {

        if(strcmp(temp->name, name) == 0 ) 
        {
            return temp->function->list;
        }

        prev = temp;
        temp = temp->next;
    }

    return NULL;
}

// Gets the syntax node of the function based on the identifiers name in the symbol table
TreeNode * GetNodeTable(LLTable * table, char * name) {
    struct LLTable *temp = table, *prev;

    while (temp != NULL) {

        if(strcmp(temp->name, name) == 0 ) 
        {
            return temp->function->node;
        }

        prev = temp;
        temp = temp->next;
    }

    return InitializeTree();
}

// Gets the function symbol table based on the identifier in the global symbol table
LLTable * getFunctionTable(LLTable * table, char * name){
    struct LLTable *temp = table, *prev;

    while (temp != NULL) {

        if(strcmp(temp->name, name) == 0 ) 
        {
            return temp->function->table;
        }

        prev = temp;
        temp = temp->next;
    }

    return NULL;
}
