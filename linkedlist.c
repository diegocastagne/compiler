/* 
    A01329863 Diego Castagne Artezan 
    A01730256 Melissa Morales Merino
*/
#include "linkedlist.h"

void InitializeList(struct LLTable * list) {
    list = NULL;
} 

void InsertIdentifier(struct LLTable** ref, char * name) {
    // Allocate memory to a node
    struct LLTable* new_node = (struct LLTable*)malloc(sizeof(struct LLTable));

    // insert the item
    new_node->name = name;

    FunctionData * item = (FunctionData*)malloc(sizeof(FunctionData));
    item->isFunction = false;
    item->args = 0;
    item->list = NULL;
    item->node = NULL;
    item->table = NULL;

    new_node->function = item;

    // Move head to new node
    new_node->next = (*ref);
    (*ref) = new_node;
}


// This method assigns a HValue to the specified number of identifiers recently added
void assignType(struct LLTable** ref, int numberOfVariables, HValue * type){
    struct LLTable *temp = *ref;

    for(int i = 0; i < numberOfVariables; i++)
    {
        HValue * tempValue;
        if(type->type == HINTEGER) tempValue = CreateInteger(0);
        else if(type->type == HBOOL) tempValue = CreateBool(0);
        else if(type->type == HFLOAT) tempValue = CreateFloat(0);
        temp->value = tempValue;
        temp = temp->next;
    }
}

void deleteSymbol(struct LLTable** ref, char * identifier) {
  struct LLTable *temp = *ref, *prev;

  if (temp != NULL && temp->name == identifier) {
    *ref = temp->next;
    free(temp);
    return;
  }
  
  while (temp != NULL && temp->name != identifier) {
    prev = temp;
    temp = temp->next;
  }


  if (temp == NULL) return;
  prev->next = temp->next;
  free(temp);
}

// Checks if the identifier is in the symbol table
bool checkIdentifier(struct LLTable** ref, char * name)
{
    struct LLTable *temp = *ref, *prev;

    while (temp != NULL) {

        if(strcmp(temp->name, name) == 0 ) return true;

        prev = temp;
        temp = temp->next;
    }
    return false;
}

// Assigns a value to the identifier provided
void assignIdentifierValue(struct LLTable** ref, char * name, HValue * value){

    struct LLTable *temp = *ref, *prev;

    while (temp != NULL) {

        if(strcmp(temp->name, name) == 0 ) 
        {
            temp->value = value;
            break;
        }

        prev = temp;
        temp = temp->next;
    }
}

// Gets the value from the symbol table
HValue * getValueFromList(struct LLTable** ref, char * name){
    struct LLTable *temp = *ref, *prev;

    while (temp != NULL) {

        if(strcmp(temp->name, name) == 0 ) 
        {
            return temp->value;
        }

        prev = temp;
        temp = temp->next;
    }

    // It shouldn't reach here
    return CreateInteger(0);
}

void deleteList(struct LLTable** head_ref) 
{ 
   struct LLTable* current = *head_ref; 
   struct LLTable* next; 
  
   while (current != NULL)  
   { 
       next = current->next; 
       free(current); 
       current = next; 
   } 
    
   *head_ref = NULL; 
} 

// Print the linked list
void printList(struct LLTable* node) {

    
    while (node != NULL) {
        if(node->function->isFunction)
        {
            printf("\n");
            printf("function ");
        }

        if(node->value->type == HINTEGER)
        {
            printf("int %s %d\n", node->name, node->value->number.intV);
        }        
        else if(node->value->type == HFLOAT)
        {
            printf("float %s %f\n", node->name, node->value->number.floatV);
        }        
        else if(node->value->type == HBOOL)
        {
            printf("bool %s %d\n", node->name, node->value->number.boolV);
        }        
        else
        {
            printf("notype %s\n", node->name);
        }       

        if(node->function->isFunction)
        {
            printList(node->function->table);
            printf("\n");
        } 
        
        node = node->next;
    }
}

// Checks if this identifier is a function
bool CheckIfFunction(LLTable * table, char * name) {
    struct LLTable *temp = table, *prev;

    while (temp != NULL) {

        if(strcmp(temp->name, name) == 0 ) 
        {
            return temp->function->isFunction;
        }

        prev = temp;
        temp = temp->next;
    }

    return false;
}

// These methods count and compare the amount of parameters to see if it's correct
bool CheckParamCount(LLTable * table, char * name, int count){
    return GetArgumentsFunction(table, name) == count;
}

int GetArgumentsFunction(LLTable * table, char * name){
    
    struct LLTable *temp = table, *prev;

    while (temp != NULL) {

        if(strcmp(temp->name, name) == 0 ) 
        {
            return temp->function->args;
        }

        prev = temp;
        temp = temp->next;
    }

    return 0;
}
