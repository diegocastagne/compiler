/* A01329863 Diego Castagne Artezan */
%{
#include <math.h>
#include "bison.tab.h"
#include "helper.h"

%}

LETRA [A-Za-z]
DIGITO [0-9]
SIMBOLO [$|_]

%%

([1-9]{DIGITO}*)|[0] {yylval.value = CreateInteger(atoi(yytext)); return NUMI;}
{DIGITO}*"."{DIGITO}+  {yylval.value = CreateFloat(atof(yytext)); return NUMR;}

"+"         {yylval.instruction = SUMA; return SUMA;}
"-"         {yylval.instruction = RESTA; return RESTA;}
"*"         {yylval.instruction = MULTI; return MULTI;}
"/"         {yylval.instruction = DIVIDE; return DIVIDE;}
";"	        {yylval.instruction = PYCOMA; return PYCOMA;}
"," 	    {yylval.instruction = COMA; return COMA;}
"("         {yylval.instruction = PARENI; return PARENI;}
")"         {yylval.instruction = PAREND; return PAREND;}
":=" 	    {yylval.instruction = DPIGUAL; return DPIGUAL;}
":" 	    {yylval.instruction = DPUNTOS; return DPUNTOS;}

"=" 	    {yylval.instruction = IGUAL; return IGUAL;}
"<" 	    {yylval.instruction = MENORQUE; return MENORQUE;}
">" 	    {yylval.instruction = MAYORQUE; return MAYORQUE;}
"<=" 	    {yylval.instruction = MENORIGUAL; return MENORIGUAL;}
">=" 	    {yylval.instruction = MAYORIGUAL; return MAYORIGUAL;}
"!"         {yylval.instruction = EXCLAM; return EXCLAM;}
"&"         {yylval.instruction = AMPER; return AMPER;}
"|"         {yylval.instruction = BOR; return BOR;}

"if" 		{yylval.instruction = IF; return IF;}
"then"      {yylval.instruction = THEN; return THEN;}
"fi"        {yylval.instruction = FI; return FI;}
"else" 		{yylval.instruction = ELSE; return ELSE;}
"while"		{yylval.instruction = WHILE; return WHILE;}
"do"        {yylval.instruction = DO; return DO;}
"repeat" 	{yylval.instruction = REPEAT; return REPEAT;}
"until"		{yylval.instruction = UNTIL; return UNTIL;}
"begin" 	{yylval.instruction = _BEGIN; return _BEGIN;}
"end"       {yylval.instruction = _END; return _END;}
"return"    {yylval.instruction = _RETURN; return _RETURN;}
"int" 		{yylval.instruction = INT; return INT;}
"real" 	    {yylval.instruction = REAL; return REAL;}
"bool"      {yylval.instruction = BOOL; return BOOL;}
"let" 		{yylval.instruction = LET; return LET;}
"program"   {yylval.instruction = PROGRAM; return PROGRAM;}
"end."      {yylval.instruction = ENDPUNTO; return ENDPUNTO;}
"print"     {yylval.instruction = PRINT; return PRINT;}
"read"      {yylval.instruction = READ; return READ;}
"True"      {yylval.value = CreateBool(true); return TRUE;} 
"False"     {yylval.value = CreateBool(false); return FALSE;}

({SIMBOLO}|{LETRA})({LETRA}|{DIGITO}|{SIMBOLO})* {yylval.identifier = strdup(yytext); return IDENT;}

.
\n


%%