# Strongly Typed Language Interpreter

Interpreter made as a project for the course Compiler Design as part of my Bachelor in Computer Science and Software Engineering.

Flex was used to make a lexical analysis and store identifiers in a symbol table. The symbol table is created using a LinkedList to store these identifiers.

Bison was used to create an internal representation of the program by building a syntax tree, a data structure made with pointers that describe the logic of the program to interpret. This is coded in C. After creating the symbol table and the syntax tree the program is interpreted.

This implementation contains strong typed checks for the identifiers.

To language used is decribed by the following grammar:
```
prog → program id opt decls opt fun decls begin opt stmts end.

opt decls → decl lst | ε

decl lst → decl ; decl lst | decl

decl → let id lst: tipo

id lst → id, id lst | id

tipo → int | real | bool

opt fun devls → fun decl lst | ε

fun decl lst → fun decl lst fun decl | fun decl

fun decl → tipo id(oparams) stmt | tipo id(oparams);

oparams → params | ε

params → param, params | param

param → id: tipo

stmt → id := expr
| id := boolexpr
| if boolexpr then stmt fi
| if boolexpr then stmt else stmt
| while boolexpr do stmt
| repeat stmt until boolexpr
| begin opt stmts end
| read id
| print expr
| return expr

opt stmts → stmt lst | ε

stmt lst → stmt lst; stmt | stmt

expr → expr + term
| expr - term
| term

term → term * f actor
| term / f actor
| f actor

f actor → ( expr )
| id
| numi
| numr
| id (opt exprs)

otp exprs → expr lst | ε

expr lst → expr lst, expr | expr

xboolexpr → boolexpr | boolterm
| ! boolterm
| boolterm

boolterm → boolterm & boolf actor

| boolf actor
boolf actor → expr < expr
| expr > expr
| expr = expr
| (boolexpr)
| True
| False
```

To compile the Interpreter you you type
```cmd
flex flex.lex
bison -d bison.y
gcc lex.yy.c bison.tab.c linkedlist.c syntaxtree.c helper.c -lfl
```

An example of a program made with this grammar is included, you can test it by typing
`./a.out <name>.txt`
where <name> can be any in the included Test folder, the interpreter will then accept or deny the program
