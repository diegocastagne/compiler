/* 
    A01329863 Diego Castagne Artezan 
    A01730256 Melissa Morales Merino
*/
%{
#include <stdlib.h>
#include <stdio.h>
#include "helper.h"
#include "linkedlist.h"
#include "syntaxtree.h"

LLTable * table;
LLTable * tableHolder;
LLTable * tempHeadHolder;
TreeNode * node;

extern FILE * yyin;
extern int yylineno;
extern char * yytext;
extern int yylex();
extern int yyerror(char const *);

bool isIdentifierUsed(char *);
void doInsertIdentifier(char *);
void doAssignValueToIdentifier(char *, HValue *);
void doAssignTypesToMultipleIdentifiers(HValue *);
HValue * doGetValue(char *);
void errorIdenftifierExists(char *);
void errorIdentifierNotFound(char *);
void errorTypeMismatch(HValue *, HValue *);
void errorNotAFunction(char *);
void errorArgumentsMismatch(char *);
int numberDeclarationList;
int argCount = 0;
%}

%union {
    int instruction;
    char * identifier;
    struct HValue * value;
    struct TreeNode * node;
    struct LinkedList * list;
}

%token<instruction> PROGRAM _BEGIN ENDPUNTO LET INT REAL BOOL IF THEN FI ELSE WHILE DO REPEAT UNTIL _END READ PRINT SUMA RESTA MULTI DIVIDE PYCOMA COMA PARENI PAREND DPIGUAL DPUNTOS IGUAL MENORQUE MAYORQUE MENORIGUAL MAYORIGUAL EXCLAM AMPER BOR _RETURN
%token<value> NUMI NUMR TRUE FALSE
%token<identifier> IDENT
%type<list> oparams params param
%type<value> tipo
%type<node> opt_stmts stmt_lst stmt expr term factor boolexpr boolterm boolfactor assign_factor opt_exprs expr_lst

%start prog

%%
prog : PROGRAM IDENT opt_decls opt_fun_decls _BEGIN opt_stmts ENDPUNTO { 
    node = $6;
    fprintf(stdout, "Accepted\n"); 
    }
;

opt_decls : decl_lst
          | %empty
;

decl_lst : decl PYCOMA decl_lst
         | decl
;

decl : LET id_lst DPUNTOS tipo { doAssignTypesToMultipleIdentifiers($4);}
;

id_lst : IDENT COMA id_lst {
            if (!isIdentifierUsed($1))
                doInsertIdentifier($1);
            else {
                errorIdenftifierExists($1); YYERROR;
            }
        }
       | IDENT {
            if (!isIdentifierUsed($1))
                doInsertIdentifier($1);
            else {
                errorIdenftifierExists($1); YYERROR;
            }
        }
;

tipo : INT { $$ = CreateInteger(0); }
     | REAL { $$ = CreateFloat(0.0); }
     | BOOL { $$ = CreateBool(false); }
;

opt_fun_decls : fun_decl_lst
              | %empty
;

fun_decl_lst : fun_decl_lst fun_decl 
             | fun_decl
;

table_subroutine :  %empty  { 
                // This subroutine is to be able to store symbols in each function own's table
                tableHolder = table; 
                table = NULL; 
            }
;

fun_decl : tipo IDENT table_subroutine PARENI oparams PAREND opt_decls {
            // This midrule is for the function to know about itself
            if (!isIdentifierUsed($2))
                InsertFunctionTable(&tableHolder, $2, $1, $5, NULL, table);
            else {
                errorIdenftifierExists($2); YYERROR;
            }

            table = tableHolder;
            tableHolder = NULL;

            // Concatenatign the tables for the stmt part to know about all identifiers
            tempHeadHolder = table;
            concatenateTables(&table, getFunctionTable(table, $2));
         } stmt {

            separateTables(&table, tempHeadHolder);
            InsertStmtFunction(&table, $2, $9);

         }
         | tipo IDENT table_subroutine PARENI oparams PAREND PYCOMA {

            if (!isIdentifierUsed($2))
                InsertFunctionTable(&tableHolder, $2, $1, $5, NULL, table);
            else {
                errorIdenftifierExists($2); YYERROR;
            }

            table = tableHolder;
            tableHolder = NULL;

         }
;

oparams : params { $$ = $1; }
        | %empty { $$ = NULL; }
;

params : param COMA params { $$ = ParamJoin($1, $3); }
       | param { $$ = $1; }
;

param : IDENT DPUNTOS tipo {
    if (!isIdentifierUsed($1)){
        doInsertIdentifier($1);
        doAssignTypesToMultipleIdentifiers($3);
    }
    else {
        errorIdenftifierExists($1); YYERROR;
    }

    $$ = ParamCreate($1, $3);
}
;

assign_factor : expr {$$ = $1;}
              | boolexpr {$$ = $1;}

stmt : IDENT DPIGUAL assign_factor {
        if (!isIdentifierUsed($1)){
            errorIdentifierNotFound($1); 
            YYERROR;
        }

        TreeNode * id_node;
        char * identifier = $1;
        HValue * value = getValueFromList(&table, identifier);
        id_node = CreateValueTree(TIDENTIFIER, identifier, value);

        $$ = CreateAssign(id_node, $3, NULL);

        // type checking
        if (!CheckTypesTree(id_node, $3)) {
            errorTypeMismatch(id_node->value, $3->value);
            YYERROR;
        }
     }
     | IF boolexpr THEN stmt FI { $$ = CreateIf($2, $4, NULL); }
     | IF boolexpr THEN stmt ELSE stmt { $$ = CreateIfElse($2, $4, $6); }
     | WHILE boolexpr DO stmt { $$ = CreateWhile($2, $4, NULL); }
     | REPEAT stmt UNTIL boolexpr { $$ = CreateRepeat($2, $4, NULL); }
     | _BEGIN opt_stmts _END { $$ = $2;}
     | READ IDENT { 
         if (!isIdentifierUsed($2)){
            errorIdentifierNotFound($2); 
            YYERROR;
        }

        TreeNode * id_node; 
        char * identifier = $2;
        HValue * value = getValueFromList(&table, identifier);
        id_node = CreateValueTree(TIDENTIFIER, identifier, value);

        $$ = CreateRead(id_node, NULL, NULL);
      }
     | PRINT expr { $$ = CreatePrint($2, NULL, NULL); }
     | _RETURN expr { $$ = CreateReturn($2, NULL, NULL); }
;

opt_stmts : stmt_lst { $$ = $1;} 
          | %empty {$$ = InitializeTree();}
;

stmt_lst : stmt_lst PYCOMA stmt{ $$ = CreateStmt($1, $3, NULL); }
         | stmt { $$ = CreateStmt($1, NULL, NULL); }
;

expr : expr SUMA term {
        $$ = CreateExpr(HSUM, $1, $3, NULL);

        if (!CheckTypesTree($1, $3)) {
            errorTypeMismatch($1->value, $3->value);
            YYERROR;
        }
     }
     | expr RESTA term {
        $$ = CreateExpr(HSUB, $1, $3, NULL);

        if (!CheckTypesTree($1, $3)) {
            errorTypeMismatch($1->value, $3->value);
            YYERROR;
        }
     }
     | term { $$ = $1; }
;

term : term MULTI factor {
        $$ = CreateTerm(HMULT, $1, $3, NULL);

        if (!CheckTypesTree($1, $3)) {
            errorTypeMismatch($1->value, $3->value);
            YYERROR;
        }
     }
     | term DIVIDE factor {
        $$ = CreateTerm(HDIV, $1, $3, NULL);

        if (!CheckTypesTree($1, $3)) {
            errorTypeMismatch($1->value, $3->value);
            YYERROR;
        }
     }
     | factor { $$ = $1; }
;

factor : PARENI expr PAREND { $$ = $2; }
       | IDENT {
            if (!isIdentifierUsed($1)){
                errorIdentifierNotFound($1); 
                YYERROR;
            }

            TreeNode * id_node;
            HValue * value = getValueFromList(&table, $1);
            id_node = CreateValueTree(TIDENTIFIER, $1, value);

            $$ = id_node;
        }
       | NUMI {
            TreeNode * int_node;
            HValue * value = $1;
            int_node = CreateValueTree(TVALUE, NULL, value);

            $$ = int_node;
       }
       | NUMR {
            TreeNode * float_node;
            HValue * value = $1;
            float_node = CreateValueTree(TVALUE, NULL, value);

            $$ = float_node;
       }
       | IDENT PARENI opt_exprs PAREND{
            if (!isIdentifierUsed($1)){
                errorIdentifierNotFound($1); 
                YYERROR;
            }

            if (!CheckIfFunction(table, $1)) {
                errorNotAFunction($1);
                YYERROR;
            }

            if (!CheckParamCount(table, $1, argCount)) {
                errorArgumentsMismatch($1);
                YYERROR;
            }

            TreeNode * function_node;
            HValue * value = getValueFromList(&table, $1);
            function_node = CreateFunction($1, value, $3);
            
            argCount = 0;
            $$ = function_node;
       }
;

opt_exprs : expr_lst{ $$ = $1; }
          | %empty{ $$ = NULL; }
;

expr_lst : expr_lst COMA expr{
            // Create a node of arg.
            argCount++;
            $$ = CreateArg($1, $3, NULL);
         }
         | expr{
            argCount++;
            $$ = CreateArg($1, NULL, NULL);
         }
;

boolexpr : boolexpr BOR boolterm {
            $$ = CreateExpr(HBOR, $1, $3, NULL);

            if (!CheckTypesTree($1, $3)) {
                errorTypeMismatch($1->value, $3->value);
                YYERROR;
        }
         }
         | EXCLAM boolterm {
            $$ = CreateExpr(HEXCLAM, $2, $2, NULL);

            // should check for a boolean
         }
         | boolterm { $$ = $1;}
;

boolterm : boolterm AMPER boolfactor {
            $$ = CreateTerm(HAMPER, $1, $3, NULL);

            if (!CheckTypesTree($1, $3)) {
                errorTypeMismatch($1->value, $3->value);
                YYERROR;
            }
         }
         | boolfactor { $$ = $1; }
;

boolfactor : expr MENORQUE expr { 
                $$ = CreateTerm(HLESS, $1, $3, NULL);

                if (!CheckTypesTree($1, $3)) {
                    errorTypeMismatch($1->value, $3->value);
                    YYERROR;
                }
           }
           | expr MAYORQUE expr {
                $$ = CreateTerm(HGREATER, $1, $3, NULL);

                if (!CheckTypesTree($1, $3)) {
                    errorTypeMismatch($1->value, $3->value);
                    YYERROR;
                }
           }
           | expr IGUAL expr {
                $$ = CreateTerm(HEQUALS, $1, $3, NULL);

                if (!CheckTypesTree($1, $3)) {
                    errorTypeMismatch($1->value, $3->value);
                    YYERROR;
                }
           }
           | PARENI boolexpr PAREND { $$ = $2; }
           | TRUE { 
                TreeNode * bool_node;
                HValue * value = $1;
                bool_node = CreateValueTree(TVALUE, NULL, value);

                $$ = bool_node;
           }
           | FALSE { 
                TreeNode * bool_node;
                HValue * value = $1;
                bool_node = CreateValueTree(TVALUE, NULL, value);

                $$ = bool_node;
           }
;
%%

int yyerror(char const * error) {
    fprintf(stderr, "%s in '%s', line: %d.\n", error, yytext, yylineno); // I dunno why yylineno is not working
}

bool isIdentifierUsed(char * identifier) {
    return checkIdentifier(&table, identifier);
}

void doInsertIdentifier(char * identifier) {
    InsertIdentifier(&table, identifier);
    numberDeclarationList++;
}

void doAssignTypesToMultipleIdentifiers(HValue * type){
    assignType(&table, numberDeclarationList, type);
    numberDeclarationList = 0;
}

void errorIdenftifierExists(char * identifier) {
    char error[500] = "Identifier already exists: ";
    strcat(error, identifier);
    yyerror(error);
}

void errorTypeMismatch(HValue * one, HValue * two) {
    char error[500] = "Type mismatch";
    printf("Type mismatch between ");
    if (one->type == HINTEGER) {
        printf("int: %d and ", one->number.intV);
    }
    else if (one->type == HFLOAT) {
        printf("real: %f and ", one->number.floatV);
    }
    else if (one->type == HBOOL) {
        printf("real: %d and ", one->number.boolV);
    }

    if (two->type == HINTEGER) {
        printf("int: %d \n", one->number.intV);
    }
    else if (two->type == HFLOAT) {
        printf("real: %f \n", one->number.floatV);
    }
    else if (two->type == HBOOL) {
        printf("real: %d \n", one->number.boolV);
    }

    yyerror(error);
}

void errorNotAFunction(char * identifier) {
    char error[1000] = "called a function but found an identifier: ";
    strcat(error, identifier);
    yyerror(error);
}

void errorArgumentsMismatch(char * identifier) {
    char error[1000] = "called function with a wrong amount of parameters: ";
    strcat(error, identifier);
    yyerror(error);
}

void doAssignValueToIdentifier(char * identifier, HValue * value) {
    assignIdentifierValue(&table, identifier, value);
}

void errorIdentifierNotFound(char * identifier) {
    char error[500] = "Identifier Not Found: ";
    strcat(error, identifier);
    yyerror(error);
}

HValue * doGetValue(char * identifier) {
    return getValueFromList(&table, identifier);
}


int main(int argc, char * argv[]) {

    if (argc < 2) 
    {
        printf ("Error, falta el nombre de un archivo\n");
        exit(1);
    }

    yyin = fopen(argv[1], "r");

    if (yyin == NULL) 
    {
        printf("Error: el archivo no existe\n");
        exit(1);
    }


    // Do the thing

    //Initialization
    numberDeclarationList = 0;
    InitializeList(table);
    node = InitializeTree();

    // Parses the program
    int success = yyparse();

    // Interpretation
    if (success == 0) {
        GTable = table;
        GValue = NULL;
        RunNode(node);
    }

    printList(table);
    deleteList(&table);

    fclose(yyin);
    return 0;
}